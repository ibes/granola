<section class="<?= implode(' ', $args['classes']); ?>">
    <?php if (!empty($args['content'])) {
        echo $args['content'];
    } ?>
</section>
