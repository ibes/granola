const animateItems = document.querySelectorAll(".animate");

let animateObserver = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
        if (entry.isIntersecting) {
            entry.target.classList.add('animate--play');
        } else {
            entry.target.classList.remove('animate--play');
        }
    });
}, {
    rootMargin: "100% 0px -15% 0px",
});

animateItems.forEach((item) => {
    animateObserver.observe(item);
});
